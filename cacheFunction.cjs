function cacheFunction(callback) {
    if (typeof callback !== 'function') {
        throw Error('No Callback function has passed');
    }

    let cacheMemory = {};

    return function () {
        let allArguments = [...arguments];
        let hasParams = allArguments.join(',');
        
        if (cacheMemory[hasParams] === undefined) {
            cacheMemory[hasParams] = callback.apply(this, allArguments);
            return cacheMemory[hasParams];
        }

        return cacheMemory[hasParams];
    }
}
module.exports = cacheFunction;
