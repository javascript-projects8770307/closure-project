function counterFactory() {
    let counter = 0;
    return {
        increment: function increment() {
            counter = counter + 1;
            return counter;
        },
        decrement: function decrement() {
            counter = counter - 1;
            return counter;
        }
    }
}

module.exports = counterFactory;