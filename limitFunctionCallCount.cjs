function limitFunctionCallCount(callback, number) {
    if (typeof callback !== 'function' || number === undefined) {
        throw Error('Either callback or number has not passed');
    }
    return function (...argument) {
        if (number > 0) {
            number--;
            return callback(...argument);
        }
        return null;
    }
}

module.exports = limitFunctionCallCount;